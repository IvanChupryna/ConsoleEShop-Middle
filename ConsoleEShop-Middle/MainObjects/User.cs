﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class User
    {
        private static int nextId = 0;
        public int Id { get; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

        public User()
        {
            Id = nextId++;
        }

        public User(string name, string email, string password, bool admin = false)
        {
            Id = nextId++;
            Name = name;
            Email = email;
            Password = password;
            IsAdmin = admin;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Email: {Email}, Password: {Password}";
        }
    }
}
