﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class Product
    {
        public int Id { get; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        private static int nextId = 0;

        public Product(string name, decimal price)
        {
            Name = name;
            Price = price;
            Id = nextId++;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Price: {Price}$";
        }
    }
}
