﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle.MainObjects
{
    public enum Status
    {
        New = 1,
        CanceledByUser,
        CanceledByAdmin,
        Paid,
        Sent,
        Received,
        Completed
    }
}
