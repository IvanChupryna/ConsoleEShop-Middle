﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle.Roles
{
    public interface IRole
    {
        public string ChooseAction();
    }
}
