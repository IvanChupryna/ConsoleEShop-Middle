using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ConsoleEShop_Middle.Tests
{
    public class ConsoleEShopTest
    {
        #region Init
        private readonly List<Product> _products;
        private readonly List<Order> _orders;
        private readonly List<User> _users;
        private DBForTesting _db;

        public ConsoleEShopTest()
        {
            _products = new List<Product>()
            {
                new Product("Apple", 1.2m),
                new Product("Banana", 2),
                new Product("Orange", 1.7m),
                new Product("Grapes", 3)
            };

            _orders = new List<Order>()
            {
                new Order
                (
                    userId: 0,
                    orderedProducts: new Dictionary<Product, int>()
                    {
                        [_products[0]] = 1,
                        [_products[1]] = 5
                    },
                    DateTime.Now
                )
            };

            _users = new List<User>()
            {
                new User("Ivan", "Ivan@gmail.com", "password"),
                new User("admin", "admin@epam.com", "admin", true)
            };

            _db = new DBForTesting(_products, _orders, _users);
        }

        #endregion

        #region DataBase

        [Fact]
        public void GetAllProducts_CallMethod_ReturnListOfProducts()
        {
            //Arrange
            var expected = _products;

            //Act
            var actual = _db.GetAllProducts();

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void GetAllUsers_CallMethod_ReturnListOfUsers()
        {
            //Arrange
            var expected = _users;

            //Act
            var actual = _db.GetAllUsers();

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void GetAllOrders_CallMethod_ReturnListOfOrders()
        {
            //Arrange
            var expected = _orders;

            //Act
            var actual = _db.GetAllOrders();

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void AddOrder_InputNewOrder_IncreaseCountOfOrdersList()
        {
            //Arrange
            int expected = _db.GetAllOrders().Count() + 1;
            var orderToAdd = new Order(3, new Dictionary<Product, int>(), DateTime.Now);

            //Act
            _db.AddOrder(orderToAdd);
            int actual = _db.GetAllOrders().Count();

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void RemoveOrder_InputOrderToRemove_DecreaseCountOfOrdersList()
        {
            //Arrange
            int expected = _orders.Count() - 1;
            var orderToDelete = _orders[0];

            //Act
            _db.DeleteOrder(orderToDelete);
            int actual = _db.GetAllOrders().Count();

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void GetOrderById_InputIdOfOrder_ReturnOrder()
        {
            //Arrange
            int idOrderToRecieve = 0;
            var expected = _orders.Where(o => o.Id == idOrderToRecieve).FirstOrDefault();

            //Act
            var actual = _db.GetOrderById(idOrderToRecieve);


            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetOrdersByUserId_InputUserId_ReturnListOfOrders()
        {
            //Arrange
            int userId = 0;
            var expected = _orders.Where(o => o.UserId == userId);

            //Act
            var actual = _db.GetOrdersForUser(userId);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetProductByName_InputStringName_ReturnProduct()
        {
            //Arrange
            string name = "Apple";
            var expected = _products.Where(p => p.Name == name).FirstOrDefault();

            //Act
            var actual = _db.GetProductByName(name);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetProductById_InputProductId_ReturnProduct()
        {
            //Arrange
            int productId = 1;
            var expected = _products[productId];

            //Act
            var actual = _db.GetProductById(productId);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddProduct_InputProduct_IncreaseCountOfListProducts()
        {
            //Arrange
            Product productToAdd = new Product("", 0);
            var expected = _products.Count() + 1;

            //Act
            _db.AddProduct(productToAdd);
            var actual = _db.GetAllProducts().Count();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddUser_InputUser_IncreaseCountOfUserList()
        {
            //Arrange
            var userToAdd = _users.FirstOrDefault();
            int expected = _users.Count() + 1;

            //Act
            _db.AddUser(userToAdd);
            int actual = _db.GetAllUsers().Count();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetUserByName_InputUserName_ReturnUser()
        {
            //Arrange
            string userName = "Ivan";
            var expected = _users.FirstOrDefault(u => u.Name == userName);

            //Act
            var actual = _db.GetUserByName(userName);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetUserById_InputUserId_ReturnUser()
        {
            //Arrange
            int userId = 0;
            var expected = _users.FirstOrDefault(u => u.Id == userId);

            //Act
            var actual = _db.GetUserById(userId);

            //Assert
            Assert.Equal(expected, actual);
        }

        #endregion
    }
}
